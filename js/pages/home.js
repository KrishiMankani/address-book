$(function() {
    $('.modal').modal();

function getUrlQueryParams() {
    const queryParams = window.location.search.substring(1).split("&");
    let vars = [],hash;
    for(let i=0;i<queryParams.length;i++) {
        hash = queryParams[i].split("=");
        vars[hash[0]] = hash[1];
    }
    return vars;
}

const queryParams = getUrlQueryParams();
const operation = queryParams['op'];
const status = queryParams['status'];

if(operation === "add" && status === "success") {

    M.toast({
        html: 'Contact Addeed Succesfully',
        classes: 'green darken-1'
    });
}
else if(operation === "edit" && status === "success") {
        M.toast({
            html: 'Contact Updated Succesfully',
            classes: 'green darken 1'
        });
    }
else{
    M.toast({
        html: 'Contact Deleted Succesfully',
        classes: 'green darken 1'
    });
    }

    let delBtns = document.querySelectorAll('.delete');
            Array.from(delBtns).forEach((button)=>{
            button.addEventListener('click',()=>{
            let id = button.dataset.id;
            let agree = document.getElementById('agree');
            agree.href = '/deleteContact.php?id='+id
        })
    })
});
